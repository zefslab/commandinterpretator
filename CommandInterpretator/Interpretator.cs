﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using CommandInterpretator.Commands;

namespace CommandInterpretator
{
    /// <summary>
    /// Главный интерпретатор, запускающий выполнение инструкций в командном файле
    /// </summary>
    class Interpretator
    {
        /// <summary>
        /// Каталог с рабочими файлами используется командами для поиска файлов 
        /// </summary>
        private string _workDirectiry;

        /// <summary>
        /// Список команд, которые умеет выполнять интерпретатор
        /// </summary>
        private IEnumerable _commands; 

        public Interpretator(string workDirectiry)
        {
            if (workDirectiry == null) throw new ArgumentNullException(nameof(workDirectiry));

            _workDirectiry = workDirectiry;
            
            //инициализация фиксированного набора команд, которыми оперирует интерпретатор
            _commands = new  ICommand []
            {
                new CommandInterpretator.Commands.Copy(),
                new CommandInterpretator.Commands.Archive(), 
                new CommandInterpretator.Commands.Delete(), 
                new CommandInterpretator.Commands.Move(), 
            };
        }

        /// <summary>
        /// Запускается командный файл на выполнение списка содержащихся в тем команд
        /// </summary>
        /// <param name="commandFile"></param>
        /// <returns></returns>
        public InterpritationResult RunCommandFile(string commandFile)
        {
            
            var result = new InterpritationResult();

            FileInfo file = ReadFile(commandFile);

           
            interpritate(file);


            return result;
        }

        /// <summary>
        /// Выводит накопленные результаты с ошибками, если таковые возникали
        /// в процессе интерпретации
        /// </summary>
        public void PrintResult()
        {
            
        }

        /// <summary>
        /// Чтение файла с диска по указанному имени из предопределенного пути
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private FileInfo ReadFile(string fileName)
        {
            FileInfo file = null;
            try
            {
                //TODO
            }
            catch (FileNotFoundException ex)
            {
                
                throw;
            }

            return file;
        }

        /// <summary>
        /// Построчное чтение содержимого командного файла и выполнение этих команд
        /// </summary>
        /// <param name="file"></param>
        private void interpritate(FileInfo file)
        {
            
            //1. Считать командную строку из файла

            //2. Идентифицировать команду

            //3. Считать параметры команды

            //4. Считать опции команды

            //5. Запустить команду на выполнение в безопасном режиме и если произойдет
            //сбой то освободить занятые неуправляемые ресурсы

            //6. Если возникнет ошибка при выполнении, то скорректировать общий результат
            //и залогировать ошибку
        }
    }

    
   

    
}
