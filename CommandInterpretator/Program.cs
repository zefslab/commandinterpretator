﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandInterpretator
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Any())
            {
                //Запускаются все командные файлы, указанные в аргументах программы
                foreach (var file in args)
                {  
                    var interpretator = new Interpretator(ConfigurationManager.AppSettings.Get("workDirectory"));

                    interpretator.RunCommandFile(file);
                }
            }
            
        }
    }
}
