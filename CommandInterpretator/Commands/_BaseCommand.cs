﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandInterpretator.Commands
{
    abstract class BaseCommand
    {
        public void Run(string commandString)
        {
            _verify(commandString);
            _parse(commandString);
            _execut(commandString);
            _report(commandString);

        }

        protected abstract void _report(string commandString);

        protected abstract void _execut(string commandString);
        

        protected abstract void _parse(string commandString);

        protected abstract void _verify(string commandString);

        /// <summary>
        /// Чтение специфичного набора параметров для каждой команды
        /// Расположение и назначение каждого параметра определяемся спецификой 
        /// работы команды.
        /// </summary>
        /// <param name="commandString"></param>
        /// <returns></returns>
        protected abstract bool ReadParametrs(string commandString);

        /// <summary>
        /// Чтение опций, которые могут уточнять характер выполнения команды.
        /// Например, перемещение с перезаписью файлов.
        /// </summary>
        /// <param name="commandString"></param>
        /// <returns>false - если возникнет ошибка при интерпретации параметра</returns>
        protected abstract  bool ReadOptions(string commandString);
    }
}
