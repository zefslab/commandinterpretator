﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandInterpretator
{
    enum TotalResults
    {
        Successfully = 1,
        CommandFileNotFound = 2,
        AnyCommandNotRun = 3

    }
}
