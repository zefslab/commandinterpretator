﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandInterpretator
{
    interface ICommand
    {
        /// <summary>
        /// Наименование команды по которому они различаются в программе
        /// </summary>
        string Name { get; set; }
        
        /// <summary>
        /// Запуск команды на выполнение. Каждая команда имеет специфичную реализацию этого метода
        /// в зависимости от рода проделываемой работы
        /// </summary>
        /// <returns></returns>
        void Run(string commandString);
    }
}
