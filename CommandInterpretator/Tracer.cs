﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandInterpretator
{
    /// <summary>
    /// Трассировщик отдельных частей командной строки
    /// </summary>
    interface ITracer
    {
        /// <summary>
        /// Вычленяет из командной строки команду и пытается ее идентифицировать по имени
        /// на основании имени и списка предоставленных команд интерпретатором
        /// </summary>
        /// <param name="commandString"></param>
        /// <returns></returns>
        ICommand TraceCommand(string commandString, IEnumerable<ICommand> accessedCommands );

        /// <summary>
        /// На основе командной подстроки находится файл
        /// выполнения над ними какого либо действия
        /// </summary>
        /// <param name="commandString"></param>
        /// <returns></returns>
        FileInfo TraceFile(string commandString);

        /// <summary>
        /// На основе командной подстроки определяется группа файлов для
        /// выполнения над ними какого либо действия
        /// </summary>
        /// <param name="commandString"></param>
        /// <returns></returns>
        IEnumerable<FileInfo> TraceFiles(string commandString);

        /// <summary>
        /// На основе командной подстроки определяется директория для
        /// использования ее в какой либо команде.
        /// </summary>
        /// <param name="commandString"></param>
        /// <returns></returns>
        DirectoryInfo TraceDirectory(string commandString);

    }
}
